package com.example.student.snapcule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.student.snapchat.R;

public class ContactsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);

        FriendsListFragment contacts = new FriendsListFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, contacts).commit();
    }
}