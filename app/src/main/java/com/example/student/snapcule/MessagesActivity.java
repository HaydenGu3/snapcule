package com.example.student.snapcule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MessagesActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages);

        MessagesFragment messages = new MessagesFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.container, messages).commit();
    }
}
