package com.example.student.snapcule;


public class Constants {
    public static final String ACTION_ADD_FRIEND = "com.example.student.snapchat.ADD_FRIEND";
    public static final String ACTION_SEND_FRIEND_REQUEST = "com.example.student.snapchat.SEND_FRIEND_REQUEST";


    public static final String BROADCAST_ADD_FRIEND_SUCCESS = "com.example.student.snapchat.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_ADD_FRIEND_FAILURE = "com.example.student.snapchat.ADD_FRIEND_FAILURE";

    public static final String BROADCAST_FRIEND_REQUEST_SUCCESS = "com.example.student.snapchat.ADD_FRIEND_SUCCESS";
    public static final String BROADCAST_FRIEND_REQUEST_FAILURE = "com.example.student.snapchat.ADD_FRIEND_FAILURE";


}