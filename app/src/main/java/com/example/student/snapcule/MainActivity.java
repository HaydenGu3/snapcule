package com.example.student.snapcule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.backendless.Backendless;
import com.example.student.snapchat.R;


import java.lang.Override;import java.lang.String;

public class MainActivity extends AppCompatActivity {

    public static final String APP_ID = "A8FA12B5-270C-9049-FFBC-18B50B234300";
    public static final String SECRET_KEY = "C6BACC2F-F272-99FF-FFB4-06CD2D2CDB00";
    public static final String VERSION = "v1";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Backendless.initApp(this, APP_ID, SECRET_KEY, VERSION);
        if (Backendless.UserService.loggedInUser() == "") {
            MainMenuFragment mainMenu = new MainMenuFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, mainMenu).commit();
        } else {
            LoggedInFragment loggedIn = new LoggedInFragment();
            getSupportFragmentManager().beginTransaction().add(R.id.container, loggedIn).commit();
        }
    }

}
