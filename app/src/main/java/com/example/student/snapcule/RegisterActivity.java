package com.example.student.snapcule;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.student.snapchat.R;


public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        RegisterFragment register = new RegisterFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.registerContainer, register).commit();
    }
}
